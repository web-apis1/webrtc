import express from 'express'
import { Server } from 'socket.io'
import { createServer } from 'http'

const app = express()
const http = createServer(app)
const io = new Server(http)

// express routing
app.use(express.static('public'))

io.on('connection', handleConnection)


// broadcasterId => socket
const broadcasters = new Map()
const viewers = new Map()

app.get('/broadcasters', (req, res) => {
    // console.log('/broadcasters',Array.from(broadcasters.keys()), broadcasters)
    res.json(Array.from(broadcasters.keys()))
})

function handleConnection(socket) {
    console.log('CONNECTED', socket.id)
    socket.on('create-broadcast', ({ broadcasterId }) => { 
        if(! broadcasters.has(broadcasterId)) { // inform all connected socket clients about new broadcaster ( just in case it's not the reconnected one )
            io.emit('new-broadcaster', { broadcasterId })
        }
        
        broadcasters.set(broadcasterId, socket) // set current valid socket that corresponds to 'broadcasterId'
    })
    socket.on('offer-sdp', ({broadcasterId, viewerId, offer}) => {
        if(broadcasters.has(broadcasterId) && viewers.has(viewerId)) { 
            const viewerSocket = viewers.get(viewerId)
            viewerSocket.emit('broadcaster-sdp-offer', { broadcasterId, offer })
        }
    })
    socket.on('offer-ice-candidate', ({broadcasterId, viewerId, iceCandidate}) => { 
        console.log('-------------- [offfer-ice-candidate]')

        if(broadcasters.has(broadcasterId) && viewers.has(viewerId)) { 
            const viewerSocket = viewers.get(viewerId)
            viewerSocket.emit('broadcaster-ice-candidate', { iceCandidate })
        }
     })

    socket.on('join-broadcast', ({broadcasterId, viewerId}) => {
        // inform broadcaster about new viewer to start SDP negotiation
        viewers.set(viewerId, socket)

        if(broadcasters.has(broadcasterId)) {
            broadcasters.get(broadcasterId).emit('new-viewer', { viewerId })
        }
    })

    socket.on('answer-sdp', ({broadcasterId, viewerId, answer}) => { 
        if(broadcasters.has(broadcasterId) && viewers.has(viewerId)) { 
            const broadcasterSocket = broadcasters.get(broadcasterId)
            broadcasterSocket.emit('viewer-sdp-answer', { answer })
        }
    })

    socket.on('answer-ice-candidate', ({broadcasterId, viewerId, iceCandidate}) => { 
        console.log('-------------- [answer-ice-candidate]')
        if(broadcasters.has(broadcasterId) && viewers.has(viewerId)) { 
            const broadcasterSocket = broadcasters.get(broadcasterId)
            broadcasterSocket.emit('viewer-ice-candidate', { iceCandidate })
        }
    })

    socket.on('leave-broadcast', ({broadcasterId}) => { })
}

 
http.listen(8111)