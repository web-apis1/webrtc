const signalingSocket = io("localhost:8111");

const videoElem = document.getElementById('stream')
const startStreamBtn = document.getElementById('start-stream-btn')
const broadcastersList = document.getElementById('broadcasters')
const broadcasters = new Set()
const VIEWER_ID = 7; // HARDCODED FOR TESTING (todo: get from server after auth ?)

function createBroadcasterElem(broadcasterId) {
    const wrapperElem = document.createElement('li')
    const nameElem = document.createElement('h2')
    const watchBtnElem = document.createElement('button')

    nameElem.textContent = `Camera ${broadcasterId}`
    watchBtnElem.textContent = 'Watch Stream'

    watchBtnElem.addEventListener('click', () => {
        watchBroadcasterStream(broadcasterId)
    })

    wrapperElem.append(nameElem, watchBtnElem)
    return wrapperElem
}


// for both - connection && reconnection
signalingSocket.on('connect', async () => {
    console.log('CONNECTED')

    const broadcasters = await fetch('/broadcasters').then(response => response.json())
    console.log(broadcasters)
    if(Array.isArray(broadcasters)) { 
        broadcastersList.innerHTML = ''
        broadcasters.forEach(broadcasterId => broadcastersList.appendChild( createBroadcasterElem(broadcasterId) ))
    }
})

signalingSocket.on('new-broadcaster', ({ broadcasterId }) => {
    broadcastersList.appendChild(createBroadcasterElem(broadcasterId))
})


const peerConnectionConfig = {
    iceServers: [{"urls": "stun:stun.l.google.com:19302"}] // we are on localhost (dont need STUN :) )
}
let peerConnection;

function watchBroadcasterStream(broadcasterId) {
    console.log(`WATCH ${broadcasterId} EVENT triggered`)

    signalingSocket.emit('join-broadcast', ({broadcasterId, viewerId: VIEWER_ID}))


    // close old peerconnection
    if(peerConnection) { peerConnection.close() }

    peerConnection = new RTCPeerConnection(peerConnectionConfig)

    peerConnection.addEventListener('icecandidate', (iceEvent) => {
        console.log('!!!!!!! ----- VIEWER ICE CANDIDATE', iceEvent)
        signalingSocket.emit('answer-ice-candidate', {broadcasterId, viewerId: VIEWER_ID, iceCandidate: iceEvent.candidate})
    })

    peerConnection.addEventListener('track', (trackEvent) => {
        videoElem.srcObject = trackEvent.streams[0]
    })
} 

signalingSocket.on('broadcaster-sdp-offer', async ({broadcasterId, offer}) => {
    console.log('RECV OFFER from broadcaster', offer)

    if(peerConnection && offer) {
        const offerSDP = new RTCSessionDescription(offer)
        peerConnection.setRemoteDescription(offerSDP)
        
        const answer = await peerConnection.createAnswer()
        peerConnection.setLocalDescription(answer)
        signalingSocket.emit('answer-sdp', {broadcasterId, viewerId: VIEWER_ID, answer})
    }
})


signalingSocket.on('broadcaster-ice-candidate', ({iceCandidate}) => {
    console.log('RECV ICE CANDIDATE from broadcaster', iceCandidate)

    if(iceCandidate) {
        const candidate = new RTCIceCandidate(iceCandidate)
        peerConnection.addIceCandidate(candidate)
    }
})

