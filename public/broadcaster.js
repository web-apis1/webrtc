
const signalingSocket = io("localhost:8111");

const camaraStreamElem = document.getElementById('camera-stream')
const startStreamBtn = document.getElementById('start-stream')
const viewersCounter = document.getElementById('viewers-counter')
const viewers = new Set()

startStreamBtn.addEventListener('click', async () => {
    try {
        const cameraStream = await navigator.mediaDevices.getUserMedia({video: true})
        camaraStreamElem.srcObject = cameraStream
    } catch(e) {
        console.error('PERMISSON DENIED', e)
        alert('Enable camera permission for this site and try again')
    }
})

const BROADCASTER_ID = 13 // some unique ID of camera (13 for testing)

signalingSocket.on('connect', () => {
    signalingSocket.emit('create-broadcast', { broadcasterId: BROADCASTER_ID })
    console.log('connected to signaling server')
})

const peerConnectionConfig = {
    iceServers: [{"urls": "stun:stun.l.google.com:19302"}] // we are on localhost (dont need STUN :) )
}

signalingSocket.on('new-viewer', async ({viewerId}) => {
    viewers.add(viewerId)
    viewersCounter.textContent = `[VIEWERS: ${viewers.size}]`

    const peerConnection = new RTCPeerConnection(peerConnectionConfig)

    // stream camera video track through PeerConnection

    // !!!!! ITS REQUIRED TO ADD TRACKS first (otherwise RTCPeerConnection events are not triggered :( ))
    const cameraMediaStream = new MediaStream(camaraStreamElem.srcObject)
    cameraMediaStream.getTracks().forEach(track => peerConnection.addTrack(track, cameraMediaStream))

    // REGISTER connection negotiation events
    signalingSocket.on('viewer-sdp-answer', ({answer}) => {
        console.log('RECV ANSWER from viewer', answer)

        peerConnection.setRemoteDescription(answer)
    })

    signalingSocket.on('viewer-ice-candidate', ({iceCandidate}) => {
        console.log('RECV ICE CANDIDATE from viewer', iceCandidate)

        peerConnection.addIceCandidate(iceCandidate)
    })

    peerConnection.addEventListener('icecandidate', (iceEvent) => {
        console.log('!!!!! ICE CANDIDATE FOUND')
        signalingSocket.emit('offer-ice-candidate', {broadcasterId: BROADCASTER_ID, viewerId, iceCandidate: iceEvent.candidate})
    })

    peerConnection.addEventListener('connectionstatechange', (e) => {
        console.log('CONNECTION STATE CHANGED', (e) => console.log(e))
    })

    // trigger connection
    const offer = await peerConnection.createOffer()
    await peerConnection.setLocalDescription(offer)
    console.log(offer)
    signalingSocket.emit('offer-sdp', {broadcasterId: BROADCASTER_ID, viewerId, offer})
})




